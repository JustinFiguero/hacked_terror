// Creator: Justin Figuero
// jusfigue@uat.edu
// Bankrupting Terrorists assignment
// 7/28/2021

#include <iostream>

using namespace std;

void normal(int x, int y);  //Prototype functions, naming the functions and specifing what variables will be passing through them.
void hacked(int& x, int& y);

int main()
{
    int JoeCoffee = 18300;  //Starting variable for Joe's shop
    int TerroristAcct = 24715000; //Starting variable for Terrorist Account

    cout << "Beginning Bank Balance\n";
    cout << "Joe's Coffee Shop Balance: " << JoeCoffee << "\n"; // showing the original amounts of each variable
    cout << "Terrorist hidden Account Balance: " << TerroristAcct << "\n\n";

    cout << "Checking the normal amounts()\n";
    normal(JoeCoffee, TerroristAcct); // calling the normal function to show no changes in balance
    cout << "Joe's Coffee Shop Balance: " << JoeCoffee << "\n"; //showing the normal amount for each variable
    cout << "Terrorist hidden Account Balance: " << TerroristAcct << "\n\n";

    cout << "Hacking Balances()\n"; // calling the hacked function to swap the amounts of each variable
    hacked(JoeCoffee, TerroristAcct);
    cout << "Joe's Coffee Shop Balance: " << JoeCoffee << "\n"; //showing the sawpped amount for each variable
    cout << "Terrorist hidden Account Balance: " << TerroristAcct << "\n";
    return 0;
}

void normal(int x, int y)   //this function stores the variables with no changes at all
{
    int temp = x;
    x = y;
    y = temp;
}

void hacked(int& x, int& y) // this function stores takes the orignal variables of x and y and swaps them using &
{
    int temp = x;
    x = y;
    y = temp;
}
